package otpgen

import "testing"

func TestConvertBytesToInts(t *testing.T) {
	bytes := []byte{0, 0, 0, 0, 0, 0, 0, 255}
	intVals := ConvertBytesToInts(bytes)

	if intVals[0] != 255 {
		t.Error("Conversion failed")
		t.Error(bytes, "Converted to", intVals)
	}
}

func BenchmarkGenerateMasterSeedBytes(b *testing.B) {
	b.StartTimer()
	for i := 0; i < b.N ; i++ {
		GenerateMasterSeedBytes()
	}
	b.StopTimer()
}