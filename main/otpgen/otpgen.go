package otpgen

import (
	"sync"
	"math/rand"
	"time"
)

const (
	SeedCount = 128
	SizePerSeed = 256 // how many bytes to generate per seed
)

type Generator struct {
	randPool sync.Pool
}

func New() *Generator {
	generator := new(Generator)
	generator.randPool = sync.Pool{
		New: func() interface{} {
			return rand.New(rand.NewSource(time.Now().UnixNano()))
		},
	}
	return generator
}

func (gen *Generator) GenOtpFromSeed(seed int64) (ret []byte) {
	ret = make([]byte, SizePerSeed)
	random := gen.randPool.Get().(*rand.Rand)
	random.Seed(seed)
	random.Read(ret)
	gen.randPool.Put(random)

	return
}

func (gen *Generator) GenOtpFromSeeds(seeds []int64) (ret []byte) {
	ret = make([]byte, SizePerSeed*len(seeds))
	group := new(sync.WaitGroup)
	for i := 0; i < len(seeds); i++ {
		group.Add(1)
		go func(i int) {
			bytes := gen.GenOtpFromSeed(seeds[i])
			copy(ret[(i * SizePerSeed):((i + 1) * SizePerSeed)], bytes)
			group.Done()
		}(i)
	}
	group.Wait()

	return
}
