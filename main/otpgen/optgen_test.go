package otpgen

import "testing"

func TestGenerator_GenOtpFromSeeds(t *testing.T) {
	gen := New()
	seeds := GenerateMasterSeeds()
	t.Log("Seeds count: ", len(seeds))

	otp := gen.GenOtpFromSeeds(seeds)
	t.Log("Otp Length: ", len(otp))
	t.Log("Otp", otp)
}

func BenchmarkGenerator_GenOtpFromSeeds(b *testing.B) {
	gen := New()
	for i := 0; i < b.N; i++ {
		seeds := GenerateMasterSeeds()
		//b.Log("Seeds count: ", len(seeds))

		gen.GenOtpFromSeeds(seeds)
		//b.Log("Otp Length: ", len(otp))
		//b.Log("Otp", otp)
	}
}
