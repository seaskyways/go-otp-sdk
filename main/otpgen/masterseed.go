package otpgen

import (
	crand "crypto/rand"
	"encoding/binary"
	"log"
)

func GenerateMasterSeedBytes() []byte {
	masterSeed := make([]byte, SeedCount * 8)
	crand.Read(masterSeed)
	return masterSeed
}

func ConvertBytesToInts(bytes []byte) []int64 {
	intervals := len(bytes) / 8
	ints := make([]int64, intervals, intervals)
	for i := 0; i < intervals; i++ {
		ints[i] = int64(binary.BigEndian.Uint64(bytes[i*8 : (i+1)*8]))
	}
	return ints
}

func uniqueInt64s(int64s []int64) (unique []int64) {
	unique = make([]int64, 0, len(int64s))
	m := make(map[int64]bool)
	for _, i := range int64s {
		m[i] = true
	}
	for i := range m {
		unique = append(unique, i)
	}
	return
}

func GenerateMasterSeeds() []int64 {
	bytes := GenerateMasterSeedBytes()
	int64s := ConvertBytesToInts(bytes)

	unique := uniqueInt64s(int64s)

	if len(unique) == len(int64s) {
		return int64s
	} else {
		log.Println("Found duplicate seeds, regenerating")
		return GenerateMasterSeeds()
	}
}